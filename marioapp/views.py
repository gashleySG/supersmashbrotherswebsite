#  marioapp/views.py

#  from django.shortcuts import render

# Create your views here.

from django.views.generic import ListView

from .models import SmashCharacters

from .models import UserDB

class HomePageView(ListView):
    model = SmashCharacters
    template_name = 'home.html'