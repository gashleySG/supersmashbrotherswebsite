from django.apps import AppConfig


class MarioappConfig(AppConfig):
    name = 'marioapp'
