# marioapp/models.py

from django.db import models

# Create your models here.

class SmashCharacters(models.Model):
   name = models.CharField(max_length=40)

   charDescription = models.TextField()

   gameSeries = models.ForeignKey(
         'auth.User',
         on_delete=models.CASCADE,
   )
 
   def __str__(self):
       return self.title

class UserDB(models.Model):
   firstName = models.CharField(max_length=40)
   lastName = models.CharField(max_length=40)
   email = models.EmailField(max_length=70,blank=True, null= True, unique= True)

   def __str__(self):
       return self.title


